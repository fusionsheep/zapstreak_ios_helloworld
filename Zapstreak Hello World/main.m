//
//  main.m
//  Zapstreak Hello World
//
//  Created by Cezary Krzyżanowski on 19.11.2012.
//  Copyright (c) 2012 Fusion Sheep Sp. z o.o. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
