//
//  ViewController.h
//  Zapstreak Hello World
//
//  Created by Cezary Krzyżanowski on 19.11.2012.
//  Copyright (c) 2012 Fusion Sheep Sp. z o.o. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <Zapstreak/Zapstreak.h>

@interface DeviceTableController : UITableViewController
    <ZPRendererFinderDelegate, UITableViewDataSource> {
        
    ZPRendererFinder *_finder;
    NSMutableArray *_devices;
}

@end
