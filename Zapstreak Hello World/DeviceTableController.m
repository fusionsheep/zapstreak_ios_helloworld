//
//  ViewController.m
//  Zapstreak Hello World
//
//  Created by Cezary Krzyżanowski on 19.11.2012.
//  Copyright (c) 2012 Fusion Sheep Sp. z o.o. All rights reserved.
//

#import "DeviceTableController.h"

@interface DeviceTableController ()

@end

NSString *const CELL_ID = @"Zapstreak Device Cell";

@implementation DeviceTableController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSError *error;
    _finder = [ZPRendererFinder sharedRendererFinderWithLicenseKey:@"ios_beta" error:&error];

    if (error) {
        NSLog(@"Error creating Zapstreak device finder: %@", error);
        return;
    }
    
    NSLog(@"Successfully created the Zapstreak finder");
    
    _devices = [[NSMutableArray alloc] init];
    [_finder setDelegate:self];
    [_finder start];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ZPRendererFinderDelegate

- (void)didFind:(ZPRendererDevice*)device {
    NSLog(@"Found device %@", device);
    [_devices addObject:device];
    
    ZPRendererAction *action = [[ZPRendererAction alloc] initWithDevice:device];
    [action playWithUri:@"http://zapstreak.com/zapstreak.mp4"];
    
    [[self tableView] reloadData];
}

#pragma mark - UITableViewSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_devices count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CELL_ID];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CELL_ID];
    }
    
    ZPRendererDevice *device = [_devices objectAtIndex:[indexPath row]];
    cell.textLabel.text = [device friendlyName];
    cell.detailTextLabel.text = [device manufacturer];
    
    return cell;
}

@end
